        <section id="footer-menu" class="sections footer-menu" style="background-color: #3D4C6F">
            <div class="container">
                <div class="row">
                    <div class="footer-menu-wrapper">

                        <div class="col-md-8 col-sm-12 col-xs-12">
                        	<div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5></h5>
                                    <ul>
                                    	<li>
                                    		<span class="glyphicon glyphicon-map-marker" style="font-size:20px;"></span>
                                    		  COLLEGE OF AGRICULTURE, Krishak Nagar, Raipur, Chhattisgarh 492012, INDIRA GANDHI KRISHI VISHWAVIDYALAYA, RAIPUR

                                    	</li><hr>
                                    	<li>
                                    		<span class="glyphicon glyphicon-phone" style="font-size:20px;"></span>
                                    		&nbsp&nbsp&nbsp&nbsp                             0771-2972822

                                    	</li><hr>
                                    	<li>
                                    		<span class="glyphicon glyphicon-envelope" style="font-size:20px;"></span>
                                    		&nbsp&nbspCBPS.BIOTECH-CG@GOV.IN

                                    	</li>
                                    </ul>

                                    <!-- <p class="text-uppercase"><span class="glyphicon glyphicon-map-marker"></span>   Krishak Nagar, Raipur, Chhattisgarh 492012, INDIRA GANDHI KRISHI VISHWAVIDYALAYA, RAIPUR</p>
                                    <p class="text-uppercase">Phone: 077124 42537</p> -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Quick Links</h5>
                                    <ul>
                                        <li><a href="index.php">HOME</a></li><hr>
                                       <!--  <li><a href="">ABOUT US</a></li><hr> -->
                                        <li><a href="">PROJECTS</a></li><hr>
                                        
                                        <li><a href="tender.php">TENDERS</a></li><hr>
                                        <li><a href="gallery.php">GALLERY</a></li><hr>
                                        <li><a href="jobs.php">JOBS</a></li><hr>
                                        <li><a href="contact.php">CONTACT US</a></li><hr>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Other Related Links</h5>
                                    <ul>
                                        <li><a href="http://www.dbtindia.nic.in/" target="_blank">Department of Bio Technology, Government of India</a></li><hr>
                                        <li><a href="http://www.dst.gov.in/" target="_blank">Department of science and technology</a></li><hr>
                                        <li><a href="http://www.dsir.gov.in/" target="_blank">Department of science and industrial research</a></li><hr>
                                        <li><a href="http://bcil.nic.in/" target="_blank">Biotech consortium india limited</a></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>

                            
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="menu-item">
                                <h5><center> Here We Are</center></h5>
                                <div class="table-responsive"><iframe frameborder="0" style="border:0; min-width: 400px; min-height: 300px;" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJtTWv-sTCKDoRNqoTCvYof6g" allowfullscreen=""></iframe></div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>