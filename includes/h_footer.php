        <section id="footer-menu" class="sections footer-menu" style="background-color: #3D4C6F ">
            <div class="container">
                <div class="row">
                    <div class="footer-menu-wrapper">

                        <div class="col-md-8 col-sm-12 col-xs-12">
                        	<div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5></h5>
                                    <ul>
                                    	<li>
                                    		<span class="glyphicon glyphicon-map-marker" style="font-size:20px;"></span>&nbsp&nbsp&nbsp
                                    		
कॉलेज ऑफ़ एग्रीकल्चर, कृषक नगर, रायपुर, छत्तीसगढ़ 492012, इंदिरा गांधी कृषि विश्वविद्यालय, रायपुर  

                                    	</li><hr>
                                    	<li>
                                    		<span class="glyphicon glyphicon-phone" style="font-size:20px;"></span>
                                    		&nbsp&nbsp&nbsp&nbsp                             0771-2972822

                                    	</li><hr>
                                    	<li>
                                    		<span class="glyphicon glyphicon-envelope" style="font-size:20px;"></span>
                                    		&nbsp&nbspCBPS.BIOTECH-CG@GOV.IN

                                    	</li>
                                    </ul>

                                    <!-- <p class="text-uppercase"><span class="glyphicon glyphicon-map-marker"></span>   Krishak Nagar, Raipur, Chhattisgarh 492012, INDIRA GANDHI KRISHI VISHWAVIDYALAYA, RAIPUR</p>
                                    <p class="text-uppercase">Phone: 077124 42537</p> -->
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>
शीघ्र लिंक</h5>
                                    <ul>
                                        <li><a href="h_index.php">मुख्य पृष्ठ</a></li><hr>
                                       <!--  <li><a href="">ABOUT US</a></li><hr> -->
                                        <li><a href="h_project.php">
परियोजना</a></li><hr>
                                        
                                        <li><a href="h_tender.php">
निविदा</a></li><hr>
                                        <li><a href="h_gallery.php">गैलरी</a></li><hr>
                                        <li><a href="h_jobs.php">नौकरियां</a></li><hr>
                                        <li><a href="h_contact.php">
संपर्क</a></li><hr>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>अन्य संबंधित लिंक</h5>
                                    <ul>
                                        <li><a href="http://www.dbtindia.nic.in/" target="_blank">जैव प्रौद्योगिकी विभाग, भारत सरकार</a></li><hr>
                                        <li><a href="http://www.dst.gov.in/" target="_blank">
										विज्ञान और प्रौद्योगिकी विभाग</a></li><hr>
                                        <li><a href="http://www.dsir.gov.in/" target="_blank">
विज्ञान और औद्योगिक अनुसंधान विभाग</a></li><hr>
                                        <li><a href="http://bcil.nic.in/" target="_blank">बायोटेक कंसोर्टियम इंडिया लिमिटेड</a></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>

                            
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="menu-item">
                                <h5><center>
यहाँ हैं हम </center></h5>
                                <div class="table-responsive"><iframe frameborder="0" style="border:0; min-width: 400px; min-height: 300px;" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;q=place_id:ChIJtTWv-sTCKDoRNqoTCvYof6g" allowfullscreen=""></iframe></div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>