<section id="social" class="social"  style="background-color: #3D4C6F">
            <div class="container-fluid" >
                <div class="row">
                    <div class="social-wrapper">
                        <div class="col-md-6 col-xs-12" style="margin-top: 15px">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12" style="margin-top: 15px">
                            <div class="social-contact">
                                <a href="#"><i class="fa fa-phone"></i>0771-2972822</a>
                                <a href="#"><i class="fa fa-envelope"></i>cbps.biotech-cg@gov.in</a>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="dropdown">
                              <button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Select Language
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                <li class="disabled"><a href="#">English</a></li>
                                <li><a href="h_index.php">Hindi</a></li>
                              </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>       
        </section>

         <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div><center><img src="assets/images/logo_left5.png" class="img-responsive" style="padding-top: 10px;"></center></div>
            </div>
            <div class="col-md-7">
                <div>
                    <center><h1 class="h1" style="font-family: Algerian">Chhattisgarh Biotech Promotion Society</h1></center>
                    <center><h3 class="h3" style="font-family: Algerian">Deparment Of Agriculture And Biotechnology</h3></center>
                    <center><h3 class="h4" style="font-family: Algerian">(Govt. Of Chhattisgarh)</h3></center>

                </div>
            </div>
            <div class="col-md-3">
                <div><center><img src="assets/images/cutmypic.png" class="img-responsive" style="padding-top: 5px;"></center>
                  <center><b><p>Dedicated for Biotech HRD, 
                  Innovation and Commercialization</p></b></center>
                </div>
            </div>
        </div>
        </div>

        
         <nav class="navbar navbar-default">          
            <div class="container-fluid bgcolor">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-sm-10">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                 <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">About us 
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="objective.php">Objective</a></li>
                              
                              <li><a href="guiding_force.php">Guiding Force</a></li>
                              <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Organization Structure </a>
                                <ul class="dropdown-menu">
                                  <li><a tabindex="-1" href="governing_council.php">Governing Council</a></li>
                      <li><a  href="executive_board.php">Executive Board</a></li>
                      <li><a href="cbps_officials.php">CBPS Officials</a></li>
                                </ul>
                            </li>
                            </ul>
                        </li>
                        <li><a href="project.php">projects</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">Downloads
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="policy.php">Policy</a></li>
                              <!-- <li><a href="">Gazette Notification</a></li> -->
                              <!-- <li><a href="">Orders</a></li> -->
                              <!-- <li><a href="">Circulars</a></li> -->
                              <li><a href="notification.php">Notifications</a></li>
                              <li><a href="by-laws.php">By Laws</a></li>
                            </ul>
                        </li>
                        <li><a href="tender.php">tenders</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">News & outreach
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="investment.php">Investments In Biotechnology</a></li>
                              <li><a href="latest_announcement.php">Latest Announcements</a></li>
                              <li><a href="events.php">Events</a></li>
                              
                              <li><a href="gallery.php">Image Gallery</a></li>
                              
                            </ul>
                        </li>
                        <li><a href="jobs.php">jobs</a></li>
                        <li><a href="contact.php">contact</a></li>                       
                    </ul>
                </div><!-- /.navbar-collapse -->


                </div>
              </div>
                <!-- Brand and toggle get grouped for better mobile display -->
                
               
            </div><!-- /.container-fluid -->
        </nav>





          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>