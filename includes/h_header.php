<section id="social" class="social"  style="background-color: #3D4C6F">
            <div class="container-fluid" >
                <div class="row">
                    <div class="social-wrapper">
                        <div class="col-md-6 col-xs-12 col-sm-12" style="margin-top: 15px">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 col-sm-12" style="margin-top: 15px">
                            <div class="social-contact">
                                <a href="#"><i class="fa fa-phone"></i>0771-2972822</a>
                                <a href="#"><i class="fa fa-envelope"></i>cbps.biotech-cg@gov.in</a>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12 col-sm-12">
                            <div class="dropdown">
                              <button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">भाषा चुनिए
                              <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                <li class="disabled"><a href="#">Hindi</a></li>
                                <li><a href="index.php">English</a></li>
                              </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>       
        </section>

         <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div><center><img src="assets/images/logo_left5.png" class="img-responsive" style="padding-top: 10px;"></center></div>
            </div>
            <div class="col-md-7">
                <div>
                    <center><h1 class="h1">छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी </h1></center>
                    <center><h3 class="h3">कृषि और जैव प्रौद्योगिकी विभाग</h3></center>
                    <center><h3 class="h4">( छत्तीसगढ़  शासन)</h3></center>

                </div>
            </div>
            <div class="col-md-3">
                <div><center><img src="assets/images/cutmypic.png" class="img-responsive" style="padding-top: 5px;"></center>
                  <center><b><p>Dedicated for Biotech HRD, 
                  Innovation and Commercialization</p></b></center>
                </div>
            </div>
        </div>
        </div>

        
         <nav class="navbar navbar-default">          
            <div class="container-fluid bgcolor">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-sm-10">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                 <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="h_index.php">मुख्य पृष्ठ</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">
हमारे बारे में 
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="h_objective.php">
उद्देश्य</a></li>
                              
                              <li><a href="h_guiding_force.php">
गाइडिंग फोर्स</a></li>
                              <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">संगठन की संरचना </a>
                                <ul class="dropdown-menu">
                                  <li><a tabindex="-1" href="h_governing_council.php">
संचालक समिति</a></li>
                      <li><a  href="h_executive_board.php">
प्रबंध समिति</a></li>
                      <li><a href="h_cbps_officials.php">सीबीपीएस आधिकारिक</a></li>
                                </ul>
                            </li>
                            </ul>
                        </li>
                        <li><a href="h_project.php">
परियोजना</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">डाउनलोड
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="h_policy.php">
नीति</a></li>
                              <!-- <li><a href="">Gazette Notification</a></li> -->
                              <!-- <li><a href="">Orders</a></li> -->
                              <!-- <li><a href="">Circulars</a></li> -->
                              <li><a href="h_notification.php">अधिसूचना</a></li>
                              <li><a  href="h_by-laws.php">बाई लॉज़</a></li>
                            </ul>
                        </li>
                        <li><a href="h_tender.php">निविदा</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="">
समाचार और आउटरीच
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-content">
                              <li><a href="h_investment.php">जैव प्रौद्योगिकी में निवेश</a></li>
                              <li><a href="h_latest_announcement.php">
नवीनतम घोषणाएं</a></li>
                              <li><a href="h_events.php">आयोजन</a></li>
                              
                              <li><a href="h_gallery.php">गैलरी</a></li>
                              
                            </ul>
                        </li>
                        <li><a href="h_jobs.php">नियुक्ति / भर्ती</a></li>
                        <li><a href="h_contact.php">
संपर्क</a></li>                       
                    </ul>
                </div><!-- /.navbar-collapse -->


                </div>
              </div>
                <!-- Brand and toggle get grouped for better mobile display -->
                
               
            </div><!-- /.container-fluid -->
        </nav>






          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>