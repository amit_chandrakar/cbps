<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | मुख्य पृष्ठ</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">


        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>



        <!-- css slider -->

        <link rel="stylesheet" href="assets/css/plugins.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <!-- end of css slider -->

        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
                color: #3D4C6F;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
                color: #3D4C6F;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>

    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      
        <!-- Sections -->
       
<?php
include("includes/h_header.php");
?>

        <header>
            <div class="container-fluid">
<center>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="gallery/3.jpg" alt="Chania">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide 1</h3> -->
                    <p>
माननीय मंत्री श्री बृजमोहन अग्रवाल जी, कृषि और जैव प्रौद्योगिकी विभाग द्वारा छत्तीसगढ़ बायोटेक प्रमोशन सोसायटी का उद्घाटन।</p>
                </div>
            </div>
            <div class="item">
                <img src="gallery/2.jpg" alt="Chania">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide 2</h3> -->
                    <p>
माननीय मंत्री श्री बृजमोहन अग्रवाल जी, कृषि और जैव प्रौद्योगिकी विभाग द्वारा छत्तीसगढ़ बायोटेक प्रमोशन सोसायटी का उद्घाटन।</p>
                </div>
            </div>
            <div class="item">
                <img src="gallery/4.jpg"  alt="Flower">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide3</h3> -->
                    <p>
माननीय मंत्री श्री बृजमोहन अग्रवाल जी, कृषि और जैव प्रौद्योगिकी विभाग द्वारा छत्तीसगढ़ बायोटेक प्रमोशन सोसायटी का उद्घाटन।</p>
                </div>
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
    </div>
    </center>

</div>
</header>

<div class="container-fluid" style="margin-top: 60px; background-image: url(assets/images/bg2.png);">
    <div class="row">
        <div class="col-md-4 ">
           <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">छत्तीसगढ़ के बारे में</h4></center></div>
                  <p class="text-justify padding v">
                    
छत्तीसगढ़ को मध्य भारत का धान का कटोरा भी कहा जाता है, जो कि मुख्य रूप से एक कृषि प्रधान राज्य है, जिसमें कृषि और संबद्ध क्षेत्रों में लगभग 80 प्रतिशत आबादी शामिल है। राज्य देश में सबसे अमीर जैव-विविधता वाले आकर्षण केंद्रों में से एक है, जहां वनों के अंतर्गत इसके भौगोलिक क्षेत्र का 44 प्रतिशत हिस्सा है। चावल मिलिंग उद्योग, वस्त्र / रेशम उद्योग, हर्बल तैयारी, टिशू कल्चर / माइक्रोप्रोपेगेशन, हाइब्रिड बीज, अमीर कृषि का उपयोग कर खाद्य प्रसंस्करण उद्योग, बागवानी संसाधन महत्वपूर्ण हैं...
                    <center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal">
और पढ़े</button></center><br>
                  </p>
                </div> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-success margin-top">अध्यक्ष / मंत्री के विज़न</h4></center></div>
                  <p class="text-justify padding">
                    राज्य में वैज्ञानिक अनुसंधान और विकास सुविधाओं, ज्ञान और कौशल आधार, प्रचुर मात्रा में जैव संसाधन, सहायक सरकारी नीतियों, लागत प्रभावशीलता और राज्यों की दृष्टि से राज्य द्वारा प्रदान किए जाने वाले कई लाभों पर विचार करते हुए कृषि, स्वास्थ्य सेवा, प्रक्रिया और पर्यावरण उद्योग को बदलने के लिए जैव प्रौद्योगिकी में भारी क्षमता है। विशाल बाजार आधार छत्तीसगढ़ में ज्ञान का निर्माण करने का एक सपना है
                    ...
                    <center><br><button class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal1">और पढ़े</button></center><br>
                  </p>

                </div> 
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">
भारत में बायोटेक परिदृश्य</h4></center></div>
                   <p class="text-justify padding">
वित्त वर्ष 2016 तक USD11 बिलियन और वित्त वर्ष 2017 तक 11.6 अरब अमरीकी डालर तक पहुंचने का अनुमान है। भारतीय नैदानिक ​​परीक्षणों, अनुबंध अनुसंधान और विनिर्माण गतिविधियों के लिए एक प्रमुख स्थान बन रहा है जो कि जैव-सेवा क्षेत्र के विकास के लिए अग्रणी है।
                    
                    <center><br><br><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal2">और पढ़े</button></center>
                    </p><br>
                    

                </div> 
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="background-image: url(assets/images/bg2.png);">
    <div class="row">
        <div class="col-md-8">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-warning">
थ्रस्ट एरिया</h4></center>
                    <center><p class="text-info">
छत्तीसगढ़ में निवेश आकर्षित करने के लिए जोर क्षेत्रों की पहचान की गई</p></center>
                  </div>
                  <div class="table-responsive">
                  <table class="table table-hover">
                      <tr>
                          <td>
कृषि जैव प्रौद्योगिकी</td>
                          <td>जैव उर्वरक और बायोपेस्टीसाइड</td>
                      </tr>
                      <tr>
                          <td>
स्वास्थ्य देखभाल जैव प्रौद्योगिकी</td>
                          <td>
                            <ul>
                                <li>आणविक डायग्नोस्टिक्स,</li>
                                <li>
औषधीय पौधा निकालने की तैयारी (फाइटो-फार्मास्यूटिकल और न्यूट्रास्युटिकल)
                                </li>
                            </ul>
                          </td>
                      </tr>
                      <tr>
                          <td>
औद्योगिक जैव प्रौद्योगिकी</td>
                          <td>
माध्यमिक कृषि उत्पादों</td>
                      </tr>
                      <tr>
                          <td>विश्लेषणात्मक परीक्षण अन्य संभावित क्षेत्रों</td>
                          <td>
                              <ul>
                                  <li>
एंजाइम उत्पादन,</li>
                                  <li>जैविक उपचार,</li>
                                  <li>
पशु टीकाएं</li>
                              </ul>
                          </td>
                      </tr>
                  </table>
                  </div>
                   
                    <p class="text-right"><center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;">और पढ़े</button></center><br></p> 

                </div> 
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top">
नवीनतम घोषणाएं</h4></center>
                  </div>
                   <marquee class="padding" behavior="scroll" height="330px" direction="up" behavior="scroll" onmouseover="stop();"  onmouseout="start();">
                        <div><h4>बायोटेक पार्क</h4><p>छत्तीसगढ़ बायोटेक पार्क डिजाइन किया गया है...<a href="">और पढ़े</a></p><hr></div>
                        <div><h4>news1</h4><p>gh aglihi</p><hr></div>
                        <div><h4>news2</h4><p>ghdthy aglihi</p><hr></div>
                    </marquee>

                </div> 
            </div>
        </div> -->
                <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">
आयोजन</h4></center>
                  </div>
                    <ul style="padding: 10px">
                        <li>
30 मार्च 2017 को जैव प्रौद्योगिकी में उद्यमशीलता विकास पर इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला। <a href="h_events.php">और पढ़े</a> </li><hr>
                        <li>
जैव प्रौद्योगिकी विभाग में उद्यमिता विकास में बायोटेक्नोलॉजी, पं. रविशंकर शुक्ला विश्वविद्यालय, रायपुर 31 मार्च 2017 को आयोजित किया।<a href="h_events.php">और पढ़े</a></li><hr>
                        
                    </ul>
                    <br><br><br>
                    <center><a href="h_events.php"><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;">और पढ़े</button></a></center><br>
                    
                    

                </div> 
            </div>
        </div>
    </div>
</div>
<section id="blog" class="blog" style="background-image: url(assets/images/bg2.png);">
            <div class="container-fluid" style="padding-top: 0px; padding-bottom: 0px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main_blog_area text-center sections">
                            <div class="main_blog_content">
                              <p><h3 style="display: inline;">
हमारी गैलरी</h3><h5 style="display: inline; float: right;"><a href="h_gallery.php"> सभी को देखें</a></h5></p>
                                <ul class="main_blog">
                                    <li class="single_blog_content" >

                                        <img src="assets/gallery/dsc_0550.jpg" class="img-thumbnail">

                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0549.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0553.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0555.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0559.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0577.jpg" class="img-thumbnail">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


<div class="container-fluid">
    <div class="row">
<!--         <div class="col-md-4">
           <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top">
नयी परियोजनाएं</h4></center></div>
                  <marquee class="padding" behavior="scroll" height="280px" direction="up" behavior="scroll" onmouseover="stop();"  onmouseout="start();">
                        <div><h4>बायोटेक पार्क</h4><p>छत्तीसगढ़ बायोटेक पार्क डिजाइन किया गया है...<a href="">और पढ़े</a></p><hr></div>
                        <div><h4>बायोटेक पार्क</h4><p>छत्तीसगढ़ बायोटेक पार्क डिजाइन किया गया है...<a href="">और पढ़े</a></p><hr></div>
                        <div><h4>बायोटेक पार्क</h4><p>छत्तीसगढ़ बायोटेक पार्क डिजाइन किया गया है...<a href="">और पढ़े</a></p><hr></div>
                    </marquee>
                </div> 
            </div>
        </div> -->
<!--         <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-success margin-top">प्रेस विज्ञप्ति</h4></center></div>
                  <p class="text-justify padding">
                    
राज्य में वैज्ञानिक अनुसंधान और विकास सुविधाओं, ज्ञान और कौशल आधार, प्रचुर मात्रा में जैव संसाधन, सहायक सरकारी नीतियों, लागत प्रभावशीलता और राज्यों की दृष्टि से राज्य द्वारा प्रदान किए जाने वाले कई लाभों पर विचार करते हुए कृषि, स्वास्थ्य सेवा, प्रक्रिया और पर्यावरण उद्योग को बदलने के लिए जैव प्रौद्योगिकी में भारी क्षमता है। विशाल बाजार आधार छत्तीसगढ़ में ज्ञान का निर्माण करने का एक सपना है
                    ...
                    <center><br><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;">और पढ़े</button></center><br>
                  </p>

                </div> 
            </div>
        </div> -->
<!--         <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">
आयोजन</h4></center>
                  </div>
                    <ul style="padding: 10px">
                        <li>
30 मार्च 2017 को जैव प्रौद्योगिकी में उद्यमशीलता विकास पर इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला। <a href="">और पढ़े</a> </li><hr>
                        <li>
जैव प्रौद्योगिकी विभाग में उद्यमिता विकास में बायोटेक्नोलॉजी, पं। रविशंकर शुक्ला विश्वविद्यालय, रायपुर 31 मार्च 2017 को आयोजित किया।<a href="">और पढ़े</a></li><hr>
                        
                    </ul>
                    
                    <center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;">और पढ़े</button></center><br>
                    
                    

                </div> 
            </div>
        </div> -->
    </div>
</div>



<?php
include("includes/h_footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>
		<script>
		$(document).ready(function(){
		  $('.dropdown-submenu a.test').on("click", function(e){
		    $(this).next('ul').toggle();
		    e.stopPropagation();
		    e.preventDefault();
		  });
		});
		</script>

        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>

        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


<script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>

<!-- modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
छत्तीसगढ़ के बारे में</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">छत्तीसगढ़ को मध्य भारत का धान का कटोरा भी कहा जाता है, जो कि मुख्य रूप से एक कृषि प्रधान राज्य है, जिसमें कृषि और संबद्ध क्षेत्रों में लगभग 80 प्रतिशत आबादी शामिल है। राज्य देश में सबसे अमीर जैव-विविधता वाले आकर्षण केंद्रों में से एक है, जहां वनों के अंतर्गत इसके भौगोलिक क्षेत्र का 44 प्रतिशत हिस्सा है। खनन मिलिंग उद्योग, वस्त्र / रेशम उद्योग, हर्बल तैयारी, टिशू कल्चर / माइक्रोप्रोपेगेशन, हाइब्रिड बीज, समृद्ध कृषि का उपयोग करते हुए खाद्य प्रसंस्करण उद्योग, बागवानी संसाधन खनन से कहीं ज्यादा राज्य में महत्वपूर्ण और तेजी से बढ़ते उद्योग हैं। छत्तीसगढ़ जैव प्रौद्योगिकी की क्षमताओं को समृद्ध जैव-संसाधन आधार और कई विश्वविद्यालयों और अनुसंधान संस्थानों की उपस्थिति के कारण होने की एक लाभप्रद स्थिति में है, जिसमें आवश्यक तकनीकी विशेषज्ञता, ज्ञान का आधार और कला के बुनियादी ढांचे की स्थिति है।
		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
अध्यक्ष / मंत्री के विज़न</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">
राज्य में वैज्ञानिक अनुसंधान और विकास सुविधाओं, ज्ञान और कौशल आधार, प्रचुर मात्रा में जैव संसाधन, सहायक सरकारी नीतियों, लागत प्रभावशीलता और राज्यों की दृष्टि से राज्य द्वारा प्रदान किए जाने वाले कई फायदे पर विचार करने के लिए जैव प्रौद्योगिकी के क्षेत्र में कृषि, स्वास्थ्य सेवा, प्रक्रिया और पर्यावरण उद्योग को बदलने की बहुत संभावनाएं हैं। विशाल बाजार आधार छत्तीसगढ़ में छत्तीसगढ़ के लोगों के जैव प्रौद्योगिकी और विशाल प्रतिभा पूल की शक्ति का उपयोग करके
 उत्कृष्ट समाज बनाने का सपना है। बायोटेक्नोलॉजी सरकार की स्टार्ट अप इंडिया एक्शन प्लान में महत्वपूर्ण भूमिका निभाएगी। सरकार ने सहायक बायोटेक नीति की शुरुआत की और राज्य में नवाचार और उद्यमशीलता के माहौल को बनाने के लिए विनियामक बाधाओं को दूर किया। राज्य उद्यमियों का समर्थन करता है और नवाचार के सभी चरणों में विचार मंच से लेकर उत्पादों के व्यावसायीकरण तक शुरू होता है, जिससे छत्तीसगढ़ को एक वैश्विक नवप्रवर्तन केंद्र के रूप में विकसित किया जा रहा है। मैं जैव प्रौद्योगिकी के माध्यम से राज्य के सामाजिक और आर्थिक विकास में प्रभावी ढंग से योगदान करने के लिए हमारे दृष्टिकोण की प्राप्ति में आपके समर्थन के लिए अनुरोध करता हूं।

		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
<!-- modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
भारत में बायोटेक परिदृश्य</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">
वित्त वर्ष 2016 तक USD11 बिलियन और वित्त वर्ष 2017 तक 11.6 अरब अमरीकी डालर तक पहुंचने का अनुमान है। भारतीय नैदानिक ​​परीक्षणों, अनुबंध अनुसंधान और विनिर्माण गतिविधियों के लिए एक प्रमुख स्थान बन रहा है जो कि जैव-सेवा क्षेत्र के विकास के लिए अग्रणी है।


		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de;background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->





    </body>
</html>
