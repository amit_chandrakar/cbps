<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | Projects</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
       <?php
        include("includes/header.php");
        ?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.php">Home</a></li>
              <li><a href="#">Projects</a></li>
            </ul>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">Biotech Park</h4></center></div>
                  <div class="row">
                    <div class="col-md-12">
                      <p >

                        <ol style="padding:20px;">
                            <li>The Chhattisgarh Biotech Park has been designed to attract entrepreneurship in the identified thrust areas (Agriculture biotech, Healthcare biotech, Industrial biotech,Analytical testing).</li>
                            <li>This Biotech Park would provide a common platform for Industry to collaborate with Academia for technology adoption, scale up and commercialization.</li>
                            <li>This Park would provide facilities for interface of research institute with industry for better utilization of technology and available bio-resources.
                            </li>
                            <li>This Park would provide facilities for interface of research institute with industry for better utilization of technology and available bio-resources.</li>
                            <li>Mentoring the incubatees in the Biotech Park is one of the important factors for successful operation of the Biotech Park.</li>
                            <li>Chhattisgarh Biotech Park consists of Biotechnology Incubation Center (BIC) and Business Enterprise Zone (BEZ).</li>
                            <li>Accordingly, the Biotechnology Incubation Centre (BIC) will be housed within the campus of Indira Gandhi Krishi Vishwavidyalaya (IGKV) Raipur. BIC would be set up in 2 years.</li>
                            <li>Business Enterprise Zone (BEZ) will be established in 23 acres at Mungi village, Mandir Hasaud, District- Raipur (which is approximate 10 Kms from the campus). BEZ would be developed in two Phases. Phase-I will be completed within 2 years and phase-II in 3 years.</li>
                        </ol>
</p>
                      </div>
                  </div>
                </div> 
            </div>
                </div>               
                                
                
            </div>
        </div>    

        <?php
        include("includes/footer.php");
        ?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->

        <script>


(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>

    </body>
</html>
