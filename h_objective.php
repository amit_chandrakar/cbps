<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | उद्देश्य</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">



        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/gallery.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/h_header.php");
?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.html">मुख्य पृष्ठ</a></li>
              <li><a href="#">हमारे बारे में</a></li>
              <li class="active"><a href="#">उद्देश्य</a></li>
            </ul>
        </div>
        </div>


<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">
हमारा उद्देश्य</h4></center></div>
                  <ol type="1" style="font-size: 18px;"><br>
            <li style="margin-top: 5px;">
छत्तीसगढ़ राज्य में जैव प्रौद्योगिकी नीति का विकास करना |</li><br>
            <li>जैव प्रौद्योगिकी में समेकित योजना तथा कार्यक्रमों का विस्तार जिसमे संबंधित उद्योगों का विकास सम्मलित है |</li><br>
            <li> कृषि, वानिकी, पशुपालन और मत्स्य पालन क्षेत्र में जैव प्रौद्योगिकी कार्य करना, पर्यावरण संतुलन बनाये रखना |</li><br>
            <li>जैव प्रौद्योगिकी हेतु अनुसंधान और विकास को बढ़ावा देना |</li><br>
            <li>
अनुसंधान कार्यों के लिए सभी सुविधाओं के साथ राज्य और संपूर्ण विभिन्न विश्वविद्यालयों में सर्वश्रेष्ठ जैव प्रौद्योगिकी केंद्रों की स्थापना।</li><br>
            <li>
जैव प्रौद्योगिकी उद्योगों में निवेश को प्रोत्साहित करने के लिए, सर्वोत्तम बुनियादी ढांचे प्रदान करने के लिए और जैव प्रौद्योगिकी में मानव को विकसित करना |</li><br>
            <li>
विभिन्न शाखाओं जैसे सांख्यिकीय आनुवंशिकी, किम विक्रय और छवि प्रसंस्करण के साथ बायो सूचना प्रणाली का संवर्धन।</li><br>
            <li>औद्योगिक और पर्यावरण जैव प्रौद्योगिकी का विकास करना |</li><br>
            <li>
जैव प्रौद्योगिकी पार्क स्थापित करने के लिए |</li><br>
            <li>
जैव प्रौद्योगिकी द्वारा स्वास्थ्य सेवाएं और रोग नियंत्रण |</li><br>

        </ol>
                </div> 
            </div>
        </div>
        

  

      
    </div>
</div>





<?php
include("includes/h_footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
