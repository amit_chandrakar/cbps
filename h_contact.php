<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | 
संपर्क</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/h_header.php");
?>

        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.html">मुख्य पृष्ठ</a></li>
              <li><a href="#">संपर्क</a></li>
            </ul>
        </div>
        </div>

            <div class="container">
               <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top "> संपर्क</h4></center></div>
                  <div class="row">
                    <div class="card-block">
                        <br><center>
                        <img class="card-img-top" src="assets/images/Girish_Chandel.png" alt="Card image cap">
                        </center>
                        <h4 class="card-title"><center>
<br>डॉ. गिरीश चंदेल</center></h4>
                        <p class="card-text"><center>मुख्य कार्यपालन अधिकारी, छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी</center></p>
                        <p class="card-text"><center>
कृषि महाविद्यालय परिसर, कृषक नगर, रायपुर</center></p> 
                        <p class="card-text"><center>
मोबाइल- 9165368000</center></p> 
                        <p class="card-text"><center>
फ़ोन- 0771-2972822</center></p>
                        <p class="card-text"><center>
ईमेल- cbps.biotech-cg@gov.in</center></p>  <br>             
                      </div>
                  </div>
                </div>
            </div> 
            </div>
                  

            
            
            
        </div>

<?php
include("includes/h_footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>

        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->

                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>

    </body>
</html>
