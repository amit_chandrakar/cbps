<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | Governing Council</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">



        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/gallery.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/header.php");
?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.php">Home</a></li>
              <li><a href="#">Organization Structure</a></li>
              <li class="active"><a href="#">Governing Council</a></li>
            </ul>
        </div>
        </div>

<div class="container">
  <div class="row well">
    <h3><center> Governing Council Details</center></h3><hr><br>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <tr>
                <td>1</td>
                <td>Hon'ble Minister, Government of Chhattisgarh, Department of Agriculture and Biology</td>
                <td>Chairman</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Additional Chief Secretary, Govt. of Chhattisgarh, Department of Agriculture and Biology</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nominee of Secretary, Govt. of Chhattisgarh, Department of Agriculture and Biology</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Voice Chancellor, Kamdhenu University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Voice Chancellor, Indira Ghandhi Agriculture University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Voice Chancellor, Pt. Ravishankar Shukla University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>7</td>
                <td>Voice Chancellor, Bilaspur University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>8</td>
                <td>Voice Chancellor, Baster University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>9</td>
                <td>Voice Chancellor, Sarguja University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>10</td>
                <td>Voice Chancellor, AYUSH University</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>11</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Finance</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>12</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Forest</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>13</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Technical Education</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>14</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Higher Education</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>15</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Health and Family Welfare</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>16</td>
                <td>Principle Secretary / Secretary, Govt. of Chhattisgarh, Department of Commerce & Indusrties</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>17</td>
                <td>Secretary, Govt. of Chhattisgarh, Department of Agriculture and Biology</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>18</td>
                <td>Director, Agriculture, Govt. of Chhattisgarh</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>19</td>
                <td>Director, Animal Husbandries, Govt. of Chhattisgarh</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>20</td>
                <td>Director, Health Services, Govt. of Chhattisgarh</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>21</td>
                <td>Director, Horticulture, Govt. of Chhattisgarh</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>22</td>
                <td>Mohd. Aslam, Sr. Scientist, Govt. of India, Department of Biotechnology</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>23</td>
                <td>Dr. Raj Bhatnagar, ICGEB, New Delhi</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>24</td>
                <td>Dr. A. T. Dabke, Former Vice Chancellor, AYUSH University, Chhattisgarh</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>25</td>
                <td>Prof. M. L. Naik</td>
                <td>Member</td>
            </tr>
            <tr>
                <td>26</td>
                <td>Dr. Girish Chandel, Chief Executive Officer, Chhattisgarh Biotech Promotion Society</td>
                <td>Member Secretary</td>
            </tr>
            
        </table>
    </div>
  </div>
</div>
<br>




<?php
include("includes/footer.php");
?>




        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->

                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
