<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | Gallery</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">



        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/gallery.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/header.php");
?>

              <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.php">Home</a></li>
              <li><a href="#">Gallery</a></li>
            </ul>
        </div>
        </div>
        


<div class="container">
  <div class="row well">
    <h3><center> Our Gallery</center></h3><hr>
    <div class="row">
      
      <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/101.jpg"></a></div>
      <!-- <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/102.jpg"></a></div> -->
      <!-- <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/103.jpg"></a></div> -->
      <!-- <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/104.jpg"></a></div> -->
      <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/105.jpg"></a></div>
      <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/106.jpg"></a></div>
      <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/107.jpg"></a></div>
      <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/108.jpg"></a></div>
<!--    <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/102.jpg"></a></div>
    <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/103.jpg"></a></div>
    <div class="col-lg-3 col-sm-4 col-xs-12"><a href="#"><img class="thumbnail img-responsive" src="gallery/104.jpg"></a></div> -->

        
    </div>
  </div>
</div>
<div class="modal" id="myModal" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
        <button class="close" type="button" data-dismiss="modal">×</button>
        <h3 class="modal-title"></h3>
    </div>
    <div class="modal-body">
        <div id="modalCarousel" class="carousel">
 
          <div class="carousel-inner">
           
          </div>
          
          <a class="carousel-control left" href="#modaCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
          <a class="carousel-control right" href="#modalCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
          
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-info" data-dismiss="modal" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;">Close</button>
    </div>
   </div>
  </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center>
            <ul class="pagination pagination-lg">
              <li><a href="gallery.php">1</a></li>
              <li><a href="gallery-2.php">2</a></li>
              <li><a href="gallery-3.php">3</a></li>
              <li class="active"><a href="gallery-4.php">4</a></li>
              <li><a href="#">5</a></li>
            </ul>
            </center>
        </div>
    </div>
</div>


        <?php
include("includes/footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
