<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | आयोजन</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/h_header.php");
?>

        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.php">मुख्य पृष्ठ</a></li>
              <li><a href="#">समाचार और आउटरीच</a></li>
              <li><a href="#">आयोजन</a></li>
            </ul>
        </div>
        </div>

         
         <div class="container">
    <div class="row">
        <div class="col-md-4 col-xs-12 col-sm-12">
        <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top">छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी का उद्घाटन</h4></center></div>
            <p style="padding:15px">
कृषि मंत्री बृजमोहन अग्रवाल ने यहाँ कृषि महाविद्यालय परिसर में छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी
के नवीन कार्यालाय भवन का लोकार्पण किया | राज्य शासन द्वारा इस सोसाइटी का गठन जैव प्रौद्योगिकी के क्षेत्र
में नए उद्योगों की स्थापना, मानव संसाधन विकास तथा रोजगार सृजन के लिए किया गया है |</p>
<center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal1">Read More</button></center><br>
                </div> 
            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
        <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला</h4></center></div>
                  <p style="padding: 10px; ">
30 मार्च 2017 को जैव प्रौद्योगिकी में उद्यमशीलता विकास पर इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला।</p>
                  <br><br><br><br><center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal2">Read More</button></center><br>

                </div> 
            </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
        <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">
पं. रविशंकर शुक्ल विश्वविद्यालय, रायपुर में एक दिवसीय संवेदीकरण कार्यक्रम |</h4></center></div>
                  <p style="padding: 10px; ">
प्रौन्नत  विभाग,पं.  रविशंकर शुक्ल विश्वविद्यालय, रायपुर में प्रौद्योगिकी में उद्यमिता विकास पर एक दिवसीय संवेदीकरण कार्यक्रम, 31 मार्च 2017 को आयोजित किया गया।</p>
                  <br><br><center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal3">Read More</button></center><br>

                </div> 
            </div>
        </div>

        

  

      
    </div>
</div>         

<?php
include("includes/h_footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->

                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
<!-- modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी का उद्घाटन</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">

कृषि मंत्री बृजमोहन अग्रवाल ने यहाँ कृषि महाविद्यालय परिसर में छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी के नवीन कार्यालाय भवन का लोकार्पण किया | राज्य शासन द्वारा इस सोसाइटी का गठन जैव प्रौद्योगिकी के क्षेत्र में नए उद्योगों की स्थापना, मानव संसाधन विकास तथा रोजगार सृजन के लिए किया गया है |


        </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
    <!-- modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">
30 मार्च 2017 को जैव प्रौद्योगिकी में उद्यमशीलता विकास पर इंदिरा गांधी कृषि विश्वविद्यालय में एक दिवसीय कार्यशाला।


        </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
    <!-- modal -->
  <div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">पं. रविशंकर शुक्ल विश्वविद्यालय, रायपुर में एक दिवसीय संवेदीकरण कार्यक्रम |</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">प्रौन्नत विभाग,पं. रविशंकर शुक्ल विश्वविद्यालय, रायपुर में प्रौद्योगिकी में उद्यमिता विकास पर एक दिवसीय संवेदीकरण कार्यक्रम, 31 मार्च 2017 को आयोजित किया गया।


        </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
    </body>
</html>
