<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | Home</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">


        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>



        <!-- css slider -->

        <link rel="stylesheet" href="assets/css/plugins.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <!-- end of css slider -->

        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
                
            }


            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: white;
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>

    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      
        <!-- Sections -->
       
<?php
include("includes/header.php");
?>

        <header>
            <div class="container-fluid">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="gallery/3.jpg" alt="Chania">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide 1</h3> -->
                    <p>Inauguration Chhattisgarh Biotech Promotion Society by Hon'ble Minister Shri Brijmohan Agrawal Ji, Department of Agriculture and Biotechnology.</p>
                </div>
            </div>
            <div class="item">
                <img src="gallery/2.jpg" alt="Chania">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide 2</h3> -->
                    <p>Inauguration Chhattisgarh Biotech Promotion Society by Hon'ble Minister Shri Brijmohan Agrawal Ji, Department of Agriculture and Biotechnology.</p>
                </div>
            </div>
            <div class="item">
                <img src="gallery/4.jpg"  alt="Flower">
                <div class="carousel-caption">
                    <!-- <h3>Header of Slide3</h3> -->
                    <p>Inauguration Chhattisgarh Biotech Promotion Society by Hon'ble Minister Shri Brijmohan Agrawal Ji, Department of Agriculture and Biotechnology.</p>
                </div>
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
    </div>

</div>
</header>

<div class="container-fluid" style="margin-top: 60px; background-image: url(assets/images/bg2.png);">
    <div class="row">
        <div class="col-md-4 ">
           <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">About Chhattisgarh</h4></center></div>
                  <p class="text-justify padding v">
                    Chhattisgarh also known as ‘Rice Bowl of Central India is primarily an agrarian state, with about 80 percent of the population engaged in agriculture and allied sectors. The State is one of the richest bio-diversity hotspots in the country with 44 per cent of its geographical area under forests...
                    <center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal">Read More</button></center><br>
                  </p>
                </div> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-success margin-top">Vision Of Chairman/Minister</h4></center></div>
                  <p class="text-justify padding">
                    Biotechnology holds immense potential for transforming agriculture, healthcare, process and environment industry of the state considering the numerous advantages the state offers in terms of scientific research and development facilities...
                    <center><br><button class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal1">Read More</button></center><br>
                  </p>

                </div> 
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">Biotech Scenario in India</h4></center></div>
                   <p class="text-justify padding">USD11 billion by FY16 and is estimated to reach USD 11.6 billion by FY17. Indian is becoming a leading destination for clinical trials, contract research and manufacturing activities which is leading to the growth of bioservices sector."
                    
                    <center><br><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-toggle="modal" data-target="#myModal2">Read More</button></center>
                    </p><br>
                    

                </div> 
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="background-image: url(assets/images/bg2.png);">
    <div class="row">
        <div class="col-md-8">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-warning">Thrust Areas</h4></center>
                    <center><p class="text-info">Thrust areas identified for attracting investment in Chhattisgarh</p></center>
                  </div>
                  <div class="table-responsive">
                  <table class="table table-hover">
                      <tr>
                          <td>Agriculture Biotechnology</td>
                          <td>Biofertilizers and Biopesticides</td>
                      </tr>
                      <tr>
                          <td>Healthcare Biotechnology</td>
                          <td>
                            <ul>
                                <li>Molecular Diagnostics,</li>
                                <li> Medicinal Plant Extract Preparation (Phyto-pharmaceuticals
                            and nutraceuticals)
                                </li>
                            </ul>
                          </td>
                      </tr>
                      <tr>
                          <td>Industrial Biotechnology</td>
                          <td>Secondary Agriculture products</td>
                      </tr>
                      <tr>
                          <td>Analytical testing Other potential areas</td>
                          <td>
                              <ul>
                                  <li>Enzyme production,</li>
                                  <li>Bioremediation,</li>
                                  <li>Animal Vaccines</li>
                              </ul>
                          </td>
                      </tr>
                  </table>
                  </div>
                   
                    <p class="text-right"><center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;">Read More</button></center><br></p> 

                </div> 
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top">Latest Announcements</h4></center>
                  </div>
                   <marquee class="padding" behavior="scroll" height="330px" direction="up" behavior="scroll" onmouseover="stop();"  onmouseout="start();">
                        <div><h4>Biotech Park</h4><p>Chhattisgarh biotech park has been designed to...<a href="">Read More</a></p><hr></div>
                        <div><h4>news1</h4><p>gh aglihi</p><hr></div>
                        <div><h4>news2</h4><p>ghdthy aglihi</p><hr></div>
                    </marquee>

                </div> 
            </div>
        </div> -->
        <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">Events</h4></center>
                  </div>
                    <ul style="padding: 10px">
                        <li>One Day Workshop at Indira Gandhi Agriculture University on Entrepreneurship Development in Biotechnology,    on 30 th March 2017. <a href="events.php">Read more</a> </li><hr>
                        <li>One Day sensitization programme on Entrepreneurship Development in Biotechnology at Department of    Biotechnology, Pt. Ravishankar Shukla University, Raipur held on 31 st March 2017.<a href="events.php">Read more</a></li><hr>
                        
                    </ul>
                    
                    <center><a href="events.php"> <button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;">Read More</button></a></center><br>
                </div> 
            </div>
        </div>
    </div>
</div>
<section id="blog" class="blog" style="background-image: url(assets/images/bg2.png);">
            <div class="container-fluid" style="padding-top: 0px; padding-bottom: 0px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main_blog_area text-center sections">
                            <div class="main_blog_content">
                              <p><h3 style="display: inline;">Our Gallery</h3><h5 style="display: inline; float: right;"><a href="gallery.php"> View All</a></h5></p>
                                <ul class="main_blog">
                                    <li class="single_blog_content" >

                                        <img src="assets/gallery/dsc_0550.jpg" class="img-thumbnail">

                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0549.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0553.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0555.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0559.jpg" class="img-thumbnail">
                                    </li>
                                    <li class="single_blog_content">
                                        <img src="assets/gallery/dsc_0577.jpg" class="img-thumbnail">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


<div class="container-fluid" style="background-image: url(assets/images/bg2.png);">
    <div class="row">
<!--         <div class="col-md-4">
           <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top">Latest Projects</h4></center></div>
                  <marquee class="padding" behavior="scroll" height="300px" direction="up" behavior="scroll" onmouseover="stop();"  onmouseout="start();">
                        <div><h4>Biotech Park</h4><p>Chhattisgarh biotech park has been designed to...<a href="">Read More</a></p><hr></div>
                        <div><h4>Biotech Park</h4><p>Chhattisgarh biotech park has been designed to...<a href="">Read More</a></p><hr></div>
                        <div><h4>Biotech Park</h4><p>Chhattisgarh biotech park has been designed to...<a href="">Read More</a></p><hr></div>
                    </marquee>
                  

                </div> 
            </div>
        </div> -->
        <!-- <div class="col-md-4">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-primary margin-top">Events</h4></center>
                  </div>
                    <ul style="padding: 10px">
                        <li>One Day Workshop at Indira Gandhi Agriculture University on Entrepreneurship Development in Biotechnology,    on 30 th March 2017. <a href="">Read more</a> </li><hr>
                        <li>One Day sensitization programme on Entrepreneurship Development in Biotechnology at Department of    Biotechnology, Pt. Ravishankar Shukla University, Raipur held on 31 st March 2017.<a href="">Read more</a></li><hr>
                        
                    </ul>
                    
                    <center><button class="btn btn-info" style=" border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;">Read More</button></center><br>
                </div> 
            </div>
        </div> -->
    </div>
</div>



<?php
include("includes/footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>
		<script>
		$(document).ready(function(){
		  $('.dropdown-submenu a.test').on("click", function(e){
		    $(this).next('ul').toggle();
		    e.stopPropagation();
		    e.preventDefault();
		  });
		});
		</script>

        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>

        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


<script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>

<!-- modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">About Chhattisgarh</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">Chhattisgarh also known as ‘Rice Bowl of Central India is primarily an agrarian state, with about 80 percent of the population engaged in agriculture and allied sectors. The State is one of the richest bio-diversity hotspots in the country with 44 per cent of its geographical area under forests. Rice milling industries, textile/silk industries, herbal preparation, tissue culture/micropropagation, hybrid seed, food processing industries using the rich agriculture, horticulture resources are important and fast growing industries in the State other than mining. Chhattisgarh is in an advantageous position to harness the potential of biotechnology due to its strengths such as rich bio-resources base and presence of a number of Universities and Research Institutions which has requisite technical expertise, knowledge base and state of the art infrastructure.
		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de;  background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->

  <!-- modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vision of Chairman / Minister</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">Biotechnology holds immense potential for transforming agriculture, healthcare, process and environment
			industry of the state considering the numerous advantages the state offers in terms of scientific research and
			development facilities, knowledge and skill base, abundant bio-resources, supportive government policies, cost
			effectiveness and huge market base. Chhattisgarh has a dream of creating knowledge society by harnessing the
			power of biotechnology and huge talent pool of the people of Chhattisgarh. The Biotechnology will play key role
			in the government’s Start Up India Action plan. The Government heralds supportive Biotech Policy and removing
			regulatory barriers to create atmosphere of innovation and entrepreneurship in the State. State supports
			entrepreneurs and start ups in all stages of innovation starting from ideation stage to commercialization of
			products, aiming to develop Chhattisgarh as a global innovation hub. I request for your support in realization of
			our vision for effectively contributing in the social and economical development of the State through
			biotechnology.

		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
<!-- modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Biotech Scenario of India</h4>
        </div>
        <div class="modal-body">
          <p class="text-justify" style="padding: 5px;">USD11 billion by FY16 and is estimated to reach USD 11.6 billion by FY17. Indian is becoming a leading destination for clinical trials, contract research and manufacturing activities which is leading to the growth of bioservices sector."


		</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" style="border-bottom: 3px outset #5bc0de; background-color: #3D4C6F;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->





    </body>
</html>
