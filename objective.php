<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | Objective</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">



        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/gallery.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/header.php");
?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.php">Home</a></li>
              <li><a href="#">About Us</a></li>
              <li class="active"><a href="#">Objective</a></li>
            </ul>
        </div>
        </div>

<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="h3 text-info margin-top ">Objective</h4></center></div>
                  <ol type="1" style="font-size: 18px;"><br>
            <li style="margin-top: 5px;">Development of bio technology policy in chhattisgarh.</li><br>
            <li>Enhancement in project and activities under biotechnology, which including lifting up of related indusries.</li><br>
            <li>To retain ecological balance and incease productivity in agriculture, forest, animal husbandries and fisheries sectors.</li><br>
            <li>To Encourages Reasearch and Development in Biotechnology.</li><br>
            <li>Establishment of best biotechnology centers in state and to entire various universities with all facilities for reasearch works.</li><br>
            <li>To encourage investment in biotechnology indusries, to provide best infrastucure and to develop human in Biotechnology</li><br>
            <li>Enhancement of Bio information system by it's integration with various branches like statistical genetics, Kim meterices and Image Processing.</li><br>
            <li>To develop industrial and environmental bio technology.</li><br>
            <li>To establish biotechnology park.</li><br>
            <li>Health services and disease control by bio technology.</li><br>

        </ol>
                </div> 
            </div>
        </div>
        

  

      
    </div>
</div>




<?php
include("includes/footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
