<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | परियोजना</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/carousel.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
       <?php
include("includes/h_header.php");
?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.html">मुख्य पृष्ठ</a></li>
              <li><a href="#">परियोजना</a></li>
            </ul>
        </div>
        </div>
        <!-- <div class="container">
            <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">Projects</h4></center></div>
                </div> 
            </div>
            <br>
        </div> -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="panel-group min">
                <div class="panel panel-default">
                  <div class="panel-heading"><center><h4 class="text-info margin-top ">जैव-प्रौद्योगिकी पार्क परियोजना</h4></center></div>
                  <div class="row">
                    <div class="col-md-12">
                      <p >

                        <ol  style="padding:20px;">
                            <li>
छत्तीसगढ़ बायोटेक पार्क को पहचाना गया जोर क्षेत्रों (कृषि बायोटेक, हेल्थकेयर बायोटेक, औद्योगिक बायोटेक, एनालिटिकल टेस्टिंग) में उद्यमशीलता को आकर्षित करने के लिए डिजाइन किया गया है।</li>
                            <li>
यह बायोटेक पार्क प्रौद्योगिकी के लिए अकादमिक के साथ सहयोग करने, पैमाने अप और व्यावसायीकरण के लिए उद्योग के लिए एक आम मंच प्रदान करेगा।</li>
                            <li>
यह पार्क प्रौद्योगिकी के बेहतर उपयोग और उपलब्ध जैव-संसाधनों के लिए उद्योग के साथ अनुसंधान संस्थान के इंटरफेस के लिए सुविधाएं प्रदान करेगा।
                            </li>
                            <li>
यह पार्क प्रौद्योगिकी के बेहतर उपयोग और उपलब्ध जैव-संसाधनों के लिए उद्योग के साथ अनुसंधान संस्थान के इंटरफेस के लिए सुविधाएं प्रदान करेगा।</li>
                            <li>
बायोटेक पार्क में ऊष्मायनों को ध्यान में रखते हुए बायोटेक पार्क के सफल संचालन के लिए महत्वपूर्ण कारकों में से एक है।</li>
                            <li>
छत्तीसगढ़ बायोटेक पार्क में जैव प्रौद्योगिकी इन्क्यूबेशन सेंटर (बीआईसी) और बिजनेस एंटरप्राइज ज़ोन (बीईजेड) शामिल हैं।</li>
                            <li>तदनुसार, जैव प्रौद्योगिकी इन्क्यूबेशन सेंटर (बीआईसी) इंदिरा गांधी कृषि विश्वविद्यालय (आईजीकेवी) रायपुर के परिसर में स्थित होगा। बीआईसी को 2 वर्षों में स्थापित किया जाएगा।</li>
                            <li>
बिजनेस एंटरप्राइज ज़ोन (बीईजेड) मंगा गांव, मंदिर हासूड, जिला- रायपुर (जो कि परिसर से करीब 10 किलोमीटर है) में 23 एकड़ में स्थापित किया जाएगा। बीईजेड को दो चरणों में विकसित किया जाएगा। चरण- I को 3 वर्षों में 2 साल और चरण- II के भीतर पूरा किया जाएगा।</li>
                        </ol>
</p>
                      </div>
                  </div>
                </div> 
            </div>
                </div>               
                

                
                
            </div>
        </div>    



    



        <?php
include("includes/h_footer.php");
?>





        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->

                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
