<!doctype html>

<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CBPS | कार्यकारी बोर्ड</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="assets/images/cutmypic.png" type="image/x-icon"/>

        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">



        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/gallery.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            
            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-submenu {
                position: relative;
            }

            .dropdown-submenu>.dropdown-menu {
                top: 0;
                left: 100%;
            }

            .dropdown-submenu:hover>.dropdown-menu {
                display: block;
            }

            .dropdown-submenu>a:after {
                display: block;
                content: " ";
                float: right;
                width: 0;
                height: 0;
                border-color: transparent;
                border-style: solid;
                border-width: 5px 0 5px 5px;
                border-left-color: #ccc;
                margin-top: 5px;
                margin-right: -10px;
            }

            .dropdown-submenu:hover>a:after {
                border-left-color: #fff;
            }
            .v
            {
                gri
            }
            header .container-fluid
            {
                padding-left: 0px;
                padding-right: 0px;
            }


            
        </style>
    </head>
    <body style=" background-image: url(assets/images/bg2.png);">
      <?php
include("includes/h_header.php");
?>
        <div class="container">
        <div class="row">
            <ul class="breadcrumb bread">
              <li><a href="index.html">मुख्य पृष्ठ</a></li>
              <li><a href="#">संगठन संरचना</a></li>
              <li class="active"><a href="#">
कार्यकारी बोर्ड</a></li>
            </ul>
        </div>
        </div>

<div class="container">
  <div class="row well">
    <h3><center> 
कार्यकारी बोर्ड विवरण</center></h3><hr><br>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <tr>
                <td>1</td>
                <td>मुख्य कार्यकारी अधिकारी (सीईओ), छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी</td>
                <td>
अध्यक्ष</td>
            </tr>
            <tr>
                <td>2</td>
                <td>
श्री सतीश पांडे, संयुक्त सचिव, छत्तीसगढ़ सरकार , वित्त विभाग</td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>3</td>
                <td>
श्री एमिल लकड़ा, अंडर सेक्रेटरी, छत्तीसगढ़ सरकार, कृषि और जैव प्रौद्योगिकी विभाग</td>
                <td>सदस्य</td>
            </tr>          
            <tr>
                <td>4</td>
                <td>
आर. के. राठौर, अवर सचिव, छत्तीसगढ़ सरकार, कृषि और जैव प्रौद्योगिकी विभाग</td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>5</td>
                <td>
निदेशक, बागवानी, छत्तीसगढ़ सरकार </td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>6</td>
                <td>
निदेशक, स्वास्थ्य सेवा, छत्तीसगढ़ सरकार </td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>7</td>
                <td>
निदेशक, कृषि,  छत्तीसगढ़ सरकार</td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>8</td>
                <td>
निदेशक, पशुपालन, छत्तीसगढ़ सरकार </td>
                <td>सदस्य</td>
            </tr>
            <tr>
                <td>9</td>
                <td>अतिरिक्त मुख्य कार्यकारी अधिकारी, छत्तीसगढ़ जैव प्रौद्योगिकी प्रौन्नत सोसाइटी</td>
                <td>सचिव</td>
            </tr>

            
        </table>
    </div>
  </div>
</div>
<br>


<?php
include("includes/h_footer.php");
?>




        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/gallery.js"></script>
        <!-- Image slider -->
        <script src="assets/js_slider/jquery-1.11.2.min.js"></script>
        <script src="assets/js_slider/bootstrap.min.js"></script>
        <script src="assets/js_slider/plugins.js"></script>
        <script src="assets/js_slider/main.js"></script>
        <!-- end of image slider -->


                <script>
(function($){
  $(document).ready(function(){
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
      event.preventDefault(); 
      event.stopPropagation(); 
      $(this).parent().siblings().removeClass('open');
      $(this).parent().toggleClass('open');
    });
  });
})(jQuery);
/* http://www.bootply.com/nZaxpxfiXz */
</script>
    </body>
</html>
